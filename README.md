# lerna-typescript-cra-uilib-starter

Starter for Monorepos: Lerna, TypeScript, CRA and Storybook

### Notes

`postinstall` runs `build` in order to make packages visible to each other

### References

- https://dev.to/shnydercom/monorepos-lerna-typescript-cra-and-storybook-combined-4hli

- https://medium.com/@rossbulat/typescript-working-with-paths-packages-and-yarn-workspaces-6fbc7087b325

- https://www.hackernoon.com/a-startups-guide-to-approaching-in-house-modern-ui-library-pz1d356h
