import nanoid from "nanoid";
import { filter } from "fuzzaldrin";
import {
  TaskDetails,
  TaskFilters,
  TaskUpdate
} from "@react-modern-state-management/types";

export const createRepository = (seed?: { tasks: TaskDetails[] }) => {
  const loadTasks = async (): Promise<TaskDetails[]> =>
    Promise.resolve(JSON.parse(localStorage.getItem("tasks") || "[]"));

  const saveTasks = async (tasks: TaskDetails[]) =>
    Promise.resolve(localStorage.setItem("tasks", JSON.stringify(tasks)));

  if (seed) {
    for (const [key, value] of Object.entries(seed))
      if (!localStorage.getItem(key))
        localStorage.setItem(key, JSON.stringify(value));
  }

  return {
    getTasks: async (
      filters?: TaskFilters
    ): Promise<[TaskDetails[], number]> => {
      const tasks = await loadTasks();
      if (!filters) return [tasks, tasks.length];

      const {
        text,
        completed,
        categories,
        dueDate,
        offset = 0,
        limit = 15
      } = filters;
      let index = 0;
      let count = 0;
      let totalCount = 0;

      // eslint-disable-next-line complexity
      const filteredTasks = tasks.filter(task => {
        if (completed !== undefined && task.completed !== completed)
          return false;
        if (dueDate && task.dueDate && task.dueDate > dueDate) return false;
        if (
          categories &&
          task.categories.every(category => !categories.includes(category))
        )
          return false;
        if (
          text &&
          task.description &&
          !filter([task.title, task.description], text).length
        )
          return false;

        totalCount++;

        // Item is valid, now check offset and limit to return the target subset

        if (index++ < offset) return false;
        if (++count > limit) return false;
        return true;
      });

      return Promise.resolve([filteredTasks, totalCount]);
    },

    getTask: async (id: string): Promise<TaskDetails | null> => {
      const tasks = await loadTasks();
      const index = tasks.findIndex(task => task.id === id);
      return index > -1 ? tasks[index] : null;
    },

    addTask: async (task: TaskDetails): Promise<TaskDetails> => {
      const tasks = await loadTasks();
      const index =
        tasks.reduce((idx, task) => Math.max(idx, task.index), -1) + 1;
      const result = { ...task, id: nanoid(), index };
      tasks.push(result);
      await saveTasks(tasks);
      return result;
    },

    updateTask: async (
      id: string,
      task: TaskUpdate
    ): Promise<TaskDetails | null> => {
      const tasks = await loadTasks();
      const idx = tasks.findIndex(item => item.id === id);
      if (idx === -1) return null;
      const updated = { ...tasks[idx], ...task, id };
      tasks[idx] = updated;
      await saveTasks(tasks);
      return updated;
    },

    moveTask: async (
      startIndex: number,
      endIndex: number
    ): Promise<{ [key: string]: number }> => {
      const tasks = await loadTasks();

      const left = Math.min(startIndex, endIndex);
      const right = Math.max(startIndex, endIndex);
      const dir = Math.sign(startIndex - endIndex);

      const moved: { [key: string]: number } = {};

      for (const task of tasks) {
        const { id, index } = task;
        if (index === startIndex) {
          task.index = endIndex;
          moved[id] = endIndex;
        } else if (left <= index && index <= right) {
          task.index += dir;
          moved[id] = task.index;
        }
      }

      await saveTasks(tasks);
      return moved;
    },

    deleteTask: async (id: string): Promise<TaskDetails | null> => {
      let result = null;
      const tasks = await loadTasks();
      const index = tasks.findIndex(item => item.id === id);
      if (index > -1) {
        result = tasks[index];
        tasks.splice(index, 1);
        await saveTasks(tasks);
      }
      return result;
    }
  };
};

export type RepositoryType = ReturnType<typeof createRepository>;
