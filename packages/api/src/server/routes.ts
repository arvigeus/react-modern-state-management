import MockAdapter from "axios-mock-adapter";
import { TaskFilters } from "@react-modern-state-management/types";
import { RepositoryType } from "./repository";

// eslint-disable-next-line complexity
const getFilters = (url: string) => {
  const filters: TaskFilters = {
    limit: 15,
    offset: 0
  };
  const getIndex = url.indexOf("?") + 1;
  if (getIndex > 0) {
    for (const entry of url.substring(getIndex).split("&")) {
      let [key, value] = entry.split("=");
      if (!value) continue;
      key = key.trim().toLocaleLowerCase();
      value = value.trim();
      switch (key) {
        case "text":
          if (value) filters.text = value;
          break;
        case "completed":
          filters.completed = value !== "false";
          break;
        case "categories":
          const categories = value.split(",");
          filters.categories = [];
          for (const v of categories) {
            const category = v.trim();
            if (category) filters.categories.push(category);
          }
          break;
        case "duedate":
          const dueDate = new Date(value);
          if (dueDate instanceof Date) filters.dueDate = dueDate;
          break;
        case "limit":
          const limit = Number(value);
          if (limit && limit > 0) filters.limit = limit;
          break;
        case "offset":
          const offset = Number(value);
          if (offset && offset > 0) filters.offset = offset;
          break;
      }
    }
  }

  return filters;
};

export const initializeRoutes = (
  server: MockAdapter,
  store: RepositoryType
) => {
  const uuidRegex = /\/api\/tasks\/[a-z0-9_-]{21}/i;

  server.onGet(uuidRegex).reply(async ({ url = "" }) => {
    const id = url.substring(url.lastIndexOf("/") + 1);
    const task = await store.getTask(id);
    return task ? [200, { data: task }] : [404, null];
  });

  server.onGet(/\/api\/tasks\/?(\?[a-z0-9=]+)?/).reply(async ({ url }) => {
    const filters = getFilters(url || "");
    const [tasks, totalCount] = await store.getTasks(filters);
    const { limit = 15, offset = 0 } = filters;
    return [
      200,
      {
        data: tasks,
        metadata: {
          totalCount,
          limit,
          offset,
          page: Math.ceil(offset / limit) + 1,
          totalPages: Math.ceil(totalCount / limit)
        }
      }
    ];
  });

  server.onGet("/api/categories").reply(async () => {
    const [tasks] = await store.getTasks();
    const categories = Array.from(
      tasks.reduce((all, { categories }) => {
        for (const category of categories) all.add(category.toLowerCase());
        return all;
      }, new Set())
    );
    return [200, { data: categories }];
  });

  server.onPost("/api/tasks").reply(async ({ data }) => {
    const task = await store.addTask(JSON.parse(data));
    return [200, { data: task }];
  });

  // @ts-ignore
  server.onPut(uuidRegex).reply(async ({ url, data }) => {
    if (!url) return;
    const id = url.substring(url.lastIndexOf("/") + 1);
    const task = await store.updateTask(id, JSON.parse(data));
    return task
      ? [200, { data: task }]
      : [404, { data: task, errors: [new Error("Not found")] }];
  });

  server.onPut("/api/tasks/move").reply(async ({ data }) => {
    const { startIndex, endIndex } = JSON.parse(data);
    const movedTasks = await store.moveTask(startIndex, endIndex);
    return movedTasks ? [200, { data: movedTasks }] : [200, { data: {} }];
  });

  // @ts-ignore
  server.onDelete(uuidRegex).reply(async ({ url }) => {
    if (!url) return;
    const id = url.substring(url.lastIndexOf("/") + 1);
    const task = await store.deleteTask(id);
    return task
      ? [200, { data: task }]
      : [404, { data: task, errors: [new Error("Not found")] }];
  });
};
