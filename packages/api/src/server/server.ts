import { AxiosInstance } from "axios";
import MockAdapter from "axios-mock-adapter";
import * as seed from "./seed";
import { createRepository } from "./repository";
import { initializeRoutes } from "./routes";

/**
 * Initializes connection to a server
 * @param axios Instance used for connection
 */
export const initializeServer = (axios: AxiosInstance) => {
  const server = new MockAdapter(axios);
  const store = createRepository(seed);
  initializeRoutes(server, store);
};
