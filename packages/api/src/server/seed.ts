import nanoid from "nanoid";

export const tasks = [
  {
    id: nanoid(),
    index: 0,
    categories: ["app"],
    title: "Log in to add your own tasks here",
    description: "Data will be stored on your computer only",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "white"
  },
  {
    id: nanoid(),
    index: 1,
    categories: ["app"],
    title: "Click to edit task",
    description: "This will open a dialog where the task can be modified",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "green"
  },
  {
    id: nanoid(),
    index: 2,
    categories: ["app"],
    title: "Re-arrange tasks by moving them",
    description: "Drag and drop to change their position",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "red"
  },
  {
    id: nanoid(),
    index: 3,
    categories: ["app"],
    title: "Click on the button to add a new task",
    description: "Opens a dialog to add your own task",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "silver"
  },
  {
    id: nanoid(),
    index: 4,
    categories: ["dev"],
    title: "Free for hire!",
    description:
      "If you are here, then probably you are looking for a developer?",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "yellow"
  },
  {
    id: nanoid(),
    index: 5,
    categories: ["life"],
    title: "Do something that matters",
    description:
      "Be grateful for what you have, and try to make other people's lives better as well :)",
    completed: false,
    dueDate: null,
    remindMeAt: null,
    color: "blue"
  }
];
