import { ApiType } from "./Api";
import {
  TaskFilters,
  TaskNew,
  TaskUpdate
} from "@react-modern-state-management/types";

class MockApi implements ApiType {
  getTasks = (_?: TaskFilters | null) => Promise.resolve({ data: [] });
  getTask = (_: string) => Promise.resolve(null);
  getCategories = () => Promise.resolve({ data: [] });

  addTask = (task: TaskNew) => Promise.resolve({ data: { id: "", ...task } });

  updateTask = (_: string, __: TaskUpdate) => Promise.resolve(null);

  moveTask = (_: number, __: number) => Promise.resolve({});

  deleteTask = (_: string) => Promise.resolve(null);
}

export default MockApi;
