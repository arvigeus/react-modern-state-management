import { AxiosInstance } from "axios";
import getApiClient from "./getApiClient";
import { initializeServer } from "./server";
import {
  TaskDetails,
  TaskExcerpt,
  TaskFilters,
  TaskNew,
  TaskUpdate,
  TasksMoved
} from "@react-modern-state-management/types";
import { Response, ResponseList } from "./Response";

export const createApi = (apiClient: AxiosInstance) => ({
  getTasks: async (filters?: TaskFilters | null) =>
    (
      await apiClient.get<ResponseList<TaskExcerpt>>(
        `/tasks${serializeFilters(filters)}`
      )
    ).data,
  getTask: async (id: string) =>
    (await apiClient.get<Response<TaskDetails> | null>(`/tasks/${id}`)).data,
  getCategories: async () =>
    (await apiClient.get<Response<string[]>>("/categories")).data,

  addTask: async (task: TaskNew) =>
    (await apiClient.post<Response<TaskDetails>>("/tasks", task)).data,

  updateTask: async (id: string, task: TaskUpdate) =>
    (await apiClient.put<Response<TaskDetails> | null>(`/tasks/${id}`, task))
      .data,

  moveTask: async (startIndex: number, endIndex: number) =>
    (
      await apiClient.put<Response<TasksMoved>>(`/tasks/move`, {
        startIndex,
        endIndex
      })
    ).data,

  deleteTask: async (id: string) =>
    (await apiClient.delete<Response<TaskDetails> | null>(`/tasks/${id}`)).data
});

const Api = (() => {
  const client = getApiClient();
  initializeServer(client);
  return createApi(client);
})();

const serializeFilters = (filters: TaskFilters | null | undefined) => {
  const getFilters = [];
  if (filters) {
    const { text, completed, categories, dueDate, offset, limit } = filters;
    if (text) getFilters.push(`text=${encodeURI(text)}`);
    if (completed) getFilters.push(`completed=${completed}`);
    if (categories)
      getFilters.push(`categories=${encodeURI(categories.join(","))}`);
    if (dueDate) getFilters.push(`dueDate=${dueDate}`);
    if (offset && offset > 0) getFilters.push(`offset=${offset}`);
    if (limit && limit > 0) getFilters.push(`limit=${limit}`);
  }
  if (getFilters.length) return `?${getFilters.join("&")}`;
  else return "";
};

export type ApiType = ReturnType<typeof createApi>;

export default Api;
