export type Metadata = {
  totalCount: number;
  limit: number;
  offset: number;
  page: number;
  totalPages: number;
};

export type ResponseList<T> = {
  data?: T[];
  metadata?: Metadata;
  warnings?: string[];
  error?: Error;
};

export type Response<T> = {
  data?: T;
  warnings?: string[];
  error?: Error;
};

export type ResponseError = {
  id: string;
  error: Error;
};
