Everything in [/server](server) is outside the scope of this tutorial. Unless you are interested in contributing to the project, don't bother checking the code there.
