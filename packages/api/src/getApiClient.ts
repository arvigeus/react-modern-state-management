import axios, { AxiosInstance } from "axios";

const createApiClient = (): AxiosInstance =>
  axios.create({
    baseURL: "/api",
    responseType: "json",
    headers: {
      "Content-Type": "application/json"
    }
  });

export default createApiClient;
