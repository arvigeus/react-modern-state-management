module.exports = {
  transform: {
    ".(ts|tsx)": "ts-jest"
  },
  testMatch: ["<rootDir>/src/**/*.(spec|test).{ts,tsx}"],
  moduleDirectories: [
    "node_modules",
    // add the directory with the test-utils.js file:
    "src",
    "src/utils",
    __dirname // the root directory
  ],
  collectCoverageFrom: ["<rootDir>/src/**/*.{ts,tsx}"],
  coverageReporters: ["lcov", "text-summary"]
};
