import React from "react";
import UIProvider from "../src/features/UIProvider";

const UIProviderDecorator = storyFn => <UIProvider>{storyFn()}</UIProvider>;

export default UIProviderDecorator;
