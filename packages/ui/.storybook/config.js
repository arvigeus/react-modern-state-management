import { configure, addDecorator } from "@storybook/react";
import { withInfo } from "@storybook/addon-info";
import UIProviderDecorator from "./UIProviderDecorator";

// automatically import all files ending in *.stories.tsx
const req = require.context("../src", true, /.story.tsx$/);

function loadStories() {
  addDecorator(withInfo);
  addDecorator(UIProviderDecorator);
  req.keys().forEach(req);
}

configure(loadStories, module);
