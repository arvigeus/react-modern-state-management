import React from "react";
import { Fade } from "@material-ui/core";

type FadeInProps = {
  children: any;
};

const FadeIn: React.FC<FadeInProps> = ({ children }) => (
  <Fade in timeout={750}>
    {children}
  </Fade>
);

export default FadeIn;
