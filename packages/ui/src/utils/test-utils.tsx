import "@testing-library/jest-dom/extend-expect";
import React, { ReactElement } from "react";
import { RenderResult, render } from "@testing-library/react";
import UIProvider from "features/UIProvider";

const AllTheProviders: React.FC = ({ children }): ReactElement => {
  return <UIProvider>{children}</UIProvider>;
};

// @ts-ignore
const customRender = (ui: ReactElement, options?: any): RenderResult =>
  render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from "@testing-library/react";

// override render method
export { customRender as render };
