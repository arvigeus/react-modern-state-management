import React from "react";
import { FormGroup } from "@material-ui/core";
import { Box, Chip } from "@material-ui/core";
import CancelIcon from "@material-ui/icons/Cancel";

type TagListFormProps = {
  tags: string[];
  disabled?: boolean;
  name?: string;
  handleRemove: (index: number) => void;
};

const TagList: React.FC<TagListFormProps> = ({ name, handleRemove, tags }) => {
  return (
    <FormGroup row style={{ minHeight: "40px" }}>
      {tags.map((tag: string, index: number) => (
        <Box key={tag} mt={1} mr={1} display="inline-block">
          <Chip
            tabIndex={-1}
            label={tag}
            deleteIcon={<CancelIcon />}
            onDelete={handleRemove}
          />
          {name && (
            <input type="hidden" name={`${name}.${index}`} value={tag} />
          )}
        </Box>
      ))}
    </FormGroup>
  );
};

export default TagList;
