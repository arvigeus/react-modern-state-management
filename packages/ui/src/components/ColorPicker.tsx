import React, { ChangeEventHandler } from "react";
import { Box, FormGroup } from "@material-ui/core";

import { entries as colorEntries } from "@react-modern-state-management/utils";

type ColorPickerProps = {
  name?: string;
  value?: string | null;
  disabled?: boolean;
  onChange?: ChangeEventHandler;
};

const ColorPicker: React.FC<ColorPickerProps> = ({
  value,
  name,
  disabled,
  onChange
}) => (
  <FormGroup row>
    {Object.values(colorEntries).map(([colorName, colorValue]: string[]) => (
      <Box
        key={colorName}
        mt={1}
        mr={1}
        display="inline-block"
        borderRadius={2}
        border="1px solid #9a9a9a"
        bgcolor={colorValue}
        width="32px"
        height="32px"
      >
        <label style={{ width: "100%", height: "100%", display: "block" }}>
          {value === colorName && (
            <svg
              focusable="false"
              viewBox="0 0 24 24"
              aria-hidden="true"
              role="presentation"
            >
              <path
                color="black"
                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"
              />
            </svg>
          )}
          <input
            name={name}
            type="radio"
            value={colorName}
            defaultChecked={value === colorName}
            disabled={disabled}
            onChange={onChange}
            style={{ display: "none" }}
          />
        </label>
      </Box>
    ))}
  </FormGroup>
);

export default ColorPicker;
