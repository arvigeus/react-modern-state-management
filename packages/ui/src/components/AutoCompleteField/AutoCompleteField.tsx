import React, {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useReducer
} from "react";
import TextField, { TextFieldProps } from "@material-ui/core/TextField";
import { filter } from "fuzzaldrin";

import Suggestions from "./Suggestions";

type StateType = {
  suggested: string[];
  index: number;
};

const reducer = (state: StateType, action: any): StateType => {
  switch (action.type) {
    case "change": {
      const { options, value } = action;

      // Try preserving the old selected index
      const oldSelect = state.index > -1 ? state.suggested[state.index] : null;
      // Filter all suggested options based on current text
      const suggested = filter(options.suggestions, value, {
        maxResults: options.maxResults
      });
      // Try finding the index of the last selected item in the new list
      const changedIndex = oldSelect ? suggested.indexOf(oldSelect) : -1;

      return {
        suggested,
        index: changedIndex
      };
    }
    case "enter":
      return {
        suggested: [],
        index: -1
      };
    case "escape":
      return {
        suggested: state.index < 0 ? [] : state.suggested,
        index: -1
      };
    case "navigate":
      let index = state.index + action.dir;
      const maxIndex = state.suggested.length - 1;
      if (index < 0) index = maxIndex;
      else if (index > maxIndex) index = 0;
      return {
        ...state,
        index
      };
    default:
      return state;
  }
};

export type AutoCompleteFieldProps = {
  onEnter?: (value: string) => void;
  suggestions: string[];
  maxResults?: number;
} & TextFieldProps;

const AutoCompleteField: React.FC<AutoCompleteFieldProps> = ({
  onEnter = () => {},
  suggestions,
  maxResults,
  onKeyDown,
  onChange,
  onBlur,
  onFocus,
  type,
  ...props
}) => {
  const [state, dispatch] = useReducer(reducer, {
    suggested: [],
    index: -1
  });
  const { suggested, index } = state;

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      dispatch({
        type: "change",
        options: { maxResults, suggestions },
        value: e.target.value
      });
      if (onChange) onChange(e);
    },
    [maxResults, onChange, suggestions]
  );

  const handleKeyDown = useCallback(
    (e: KeyboardEvent<HTMLInputElement>) => {
      switch (e.keyCode) {
        case 13: // Enter
          e.preventDefault();
          onEnter(
            index < 0
              ? e.currentTarget.getElementsByTagName("input")[0].value
              : suggested[index]
          );
          dispatch({ type: "enter" });
          break;
        case 27: // Esc
          e.preventDefault();
          break;
        case 38: // Up
          e.preventDefault(); // Prevent selection carret from moving
          dispatch({ type: "navigate", dir: -1 });
          break;
        case 40: // Down
          e.preventDefault(); // Prevent selection carret from moving
          dispatch({ type: "navigate", dir: 1 });
          break;
      }
      if (onKeyDown) onKeyDown(e);
    },
    [onKeyDown, onEnter, index, suggested]
  );

  const handleBlur = useCallback(
    e => {
      dispatch({ type: "escape" });
      if (onBlur) onBlur(e);
    },
    [onBlur]
  );

  const handleFocus = useCallback(
    e => {
      dispatch({
        type: "change",
        options: { maxResults, suggestions },
        value: e.currentTarget.value
      });
      if (onFocus) onFocus(e);
    },
    [maxResults, onFocus, suggestions]
  );

  return (
    <>
      <TextField
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
        type="text"
        autoComplete="off"
        {...props}
      />
      <Suggestions items={suggested} index={index} handleAdd={onEnter} />
    </>
  );
};

export default AutoCompleteField;
