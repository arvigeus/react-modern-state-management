import React from "react";
import styled from "styled-components/macro";
import { MenuItem, Paper } from "@material-ui/core";
import { PaperProps } from "@material-ui/core/Paper";

type SuggestionsProps = {
  items: string[];
  index?: number;
  handleAdd: (value: string) => void;
};

const Suggestions: React.FC<SuggestionsProps> = ({
  items,
  index,
  handleAdd
}) => (
  <Menu square>
    {items.map((item, idx) => (
      <MenuItem
        key={item}
        selected={index === idx}
        data-value={item}
        onMouseDown={() => handleAdd(item)}
      >
        {item}
      </MenuItem>
    ))}
  </Menu>
);

const Menu = styled(Paper)`
  position: absolute;
  z-index: 1;
  width: 90%;
` as React.ComponentType<PaperProps>;

export default Suggestions;
