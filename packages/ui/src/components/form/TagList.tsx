import React from "react";
import { useField } from "formik";

import Tags from "../TagList";

type TagListFormProps = {
  name: string;
  disabled?: boolean;
  handleRemove: (index: number) => void;
};

const TagList: React.FC<TagListFormProps> = ({ name, ...props }) => {
  const [field] = useField(name);
  return <Tags name={name} tags={[field.value]} {...props} />;
};

export default TagList;
