import React from "react";
import { useField } from "formik";

import Picker from "../ColorPicker";

type ColorPickerProps = {
  name: string;
  disabled?: boolean;
};

const ColorPicker: React.FC<ColorPickerProps> = ({ name, disabled }) => {
  const [field] = useField(name);

  return (
    <Picker
      name={name}
      disabled={disabled}
      value={field.value}
      // eslint-disable-next-line react/jsx-handler-names
      onChange={field.onChange}
    />
  );
};

export default ColorPicker;
