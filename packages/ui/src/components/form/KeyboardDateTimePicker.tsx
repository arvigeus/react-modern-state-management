import React, { useCallback } from "react";
import {
  KeyboardDateTimePicker as MuiKeyboardDateTimePicker,
  KeyboardDateTimePickerProps as MuiKeyboardDateTimePickerProps
} from "@material-ui/pickers";
import { useField } from "formik";

interface KeyboardDateTimePickerProps
  extends Omit<MuiKeyboardDateTimePickerProps, "onChange" | "value"> {
  name: string;
  onChange: (field: string, value: any, shouldValidate?: boolean) => void;
  value?: string;
}

const KeyboardDateTimePicker: React.FC<KeyboardDateTimePickerProps> = ({
  name,
  onChange,
  ...props
}) => {
  const [field, meta] = useField(name);
  const handleChange = useCallback(value => onChange(name, value, true), [
    onChange,
    name
  ]);
  const error = meta.touched ? meta.error : null;

  return (
    <MuiKeyboardDateTimePicker
      {...field}
      {...props}
      name={name}
      onChange={handleChange}
      helperText={error}
      error={!!error}
    />
  );
};

export default KeyboardDateTimePicker;
