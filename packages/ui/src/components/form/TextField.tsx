import React from "react";
import MuiTextField, { TextFieldProps } from "@material-ui/core/TextField";
import { useField } from "formik";

const TextField: React.FC<TextFieldProps> = props => {
  const [field, meta] = useField(props.name || "");
  const error = meta.touched ? meta.error : null;

  return (
    <MuiTextField {...props} {...field} helperText={error} error={!!error} />
  );
};

export default TextField;
