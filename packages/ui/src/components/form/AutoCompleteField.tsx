import React from "react";
import { useField } from "formik";
import AutoComplete, { AutoCompleteFieldProps } from "../AutoCompleteField";

const AutoCompleteField: React.FC<AutoCompleteFieldProps> = props => {
  const [, /*{ onChange: handleChange, onBlur: handleBlur }*/ meta] = useField(
    props.name || ""
  );
  const error = meta.touched ? meta.error : null;

  return (
    <AutoComplete
      {...props}
      // onChange={handleChange}
      // onBlur={handleBlur}
      helperText={error}
      error={!!error}
    />
  );
};

export default AutoCompleteField;
