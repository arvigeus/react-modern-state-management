import React from "react";
import { FormControlLabel } from "@material-ui/core";
import MuiCheckbox, { CheckboxProps } from "@material-ui/core/Checkbox";
import { useField } from "formik";

const Checkbox: React.FC<CheckboxProps & { label: string }> = ({
  label,
  ...props
}) => {
  const [field] = useField(props.name || "");

  return (
    <FormControlLabel
      control={<MuiCheckbox {...field} {...props} />}
      label={label}
    />
  );

  //;
};

export default Checkbox;
