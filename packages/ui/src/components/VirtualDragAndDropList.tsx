import React, { useCallback } from "react";
import {
  DragDropContext,
  Draggable,
  DraggableLocation,
  DraggableProvided,
  DraggableStateSnapshot,
  Droppable,
  DroppableProvided
} from "react-beautiful-dnd";
import { FixedSizeList as List } from "react-window";
import InfiniteLoader from "react-window-infinite-loader";

import { PaginationProps } from "@react-modern-state-management/types";

export type VirtualDragAndDropItem<T> = {
  item: T;
  provided: DraggableProvided;
  isDragging: boolean;
  style?: Object;
};

type VirtualDragAndDropListProps<T> = {
  droppableId: string;
  items: T[];
  height: number;
  itemHeight: number;
  children: (props: VirtualDragAndDropItem<T>) => React.ReactElement<any>;
  onDragEnd: (oldIndex: number, newIndex: number) => void;
} & PaginationProps;

function VirtualDragAndDropList<T>({
  droppableId,
  height,
  items,
  hasNextPage,
  loadNextPage,
  isNextPageLoading,
  itemHeight,
  onDragEnd,
  children
}: VirtualDragAndDropListProps<T>) {
  const handleDragEnd = useCallback(
    ({ source, destination }) => {
      if (!destination) return;
      if (destination.index === source.index) return;

      onDragEnd(source.index, destination.index);
    },
    [onDragEnd]
  );

  // Only load 1 page of items at a time.
  // Pass an empty callback to InfiniteLoader in case it asks us to load more than once.
  const loadMoreItems = isNextPageLoading
    ? () => Promise.resolve()
    : loadNextPage;

  // Every row is loaded except for our loading indicator row.
  const isItemLoaded = (index: number) =>
    !hasNextPage || index < items.length - 1;

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      {/*
      // @ts-ignore */}
      <Droppable
        droppableId={droppableId}
        // @ts-ignore
        mode="VIRTUAL"
        whenDraggingClone={(
          provided: DraggableProvided,
          snapshot: DraggableStateSnapshot,
          source: DraggableLocation
        ) =>
          children({
            item: items[source.index],
            isDragging: snapshot.isDragging,
            provided
          })
        }
      >
        {(droppableProvided: DroppableProvided) => (
          <InfiniteLoader
            isItemLoaded={isItemLoaded}
            itemCount={items.length}
            loadMoreItems={loadMoreItems}
          >
            {({ onItemsRendered, ref }) => (
              <List
                ref={ref}
                onItemsRendered={onItemsRendered}
                width="100%"
                height={height}
                itemCount={items.length}
                itemSize={itemHeight}
                innerRef={droppableProvided.innerRef}
                itemData={items}
              >
                {({ data: items, index, style }) => {
                  const item = items[index];

                  return (
                    <>
                      <Draggable
                        draggableId={item.id}
                        index={index}
                        key={item.id}
                      >
                        {(
                          provided: DraggableProvided,
                          snapshot: DraggableStateSnapshot
                        ) =>
                          children({
                            item,
                            isDragging: snapshot.isDragging,
                            provided,
                            style
                          })
                        }
                      </Draggable>
                      {droppableProvided.placeholder}
                    </>
                  );
                }}
              </List>
            )}
          </InfiniteLoader>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default VirtualDragAndDropList;
