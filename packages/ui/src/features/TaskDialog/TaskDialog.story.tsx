import React, { useCallback } from "react";

import { storiesOf } from "@storybook/react";

import TaskDialog from "./TaskDialog";
import { exampleTasks } from "@react-modern-state-management/api";
import { TaskDetails, TaskNew } from "@react-modern-state-management/types";
import { getCategories } from "@react-modern-state-management/utils";

storiesOf("TaskDialog", module)
  .add(
    "New task",
    () => {
      const handleSubmit = useCallback((task: TaskDetails | TaskNew) => {
        console.info(task);
      }, []);

      return (
        <TaskDialog
          categorySuggestions={getCategories(exampleTasks)}
          title="New task"
          actionText="Create task"
          open
          toggle={() => {}}
          onSubmit={handleSubmit}
        />
      );
    },
    { info: { inline: true } }
  )
  .add(
    "Edit task",
    () => {
      const handleSubmit = useCallback((task: TaskDetails | TaskNew) => {
        console.info(task);
      }, []);

      const task =
        exampleTasks[Math.floor(Math.random() * exampleTasks.length)];

      return (
        <TaskDialog
          task={task}
          categorySuggestions={getCategories(exampleTasks)}
          title="Edit task"
          actionText="Submit"
          open
          toggle={() => {}}
          onSubmit={handleSubmit}
        />
      );
    },
    { info: { inline: true } }
  );
