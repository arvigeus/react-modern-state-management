import React from "react";
import styled from "styled-components/macro";
import MuiDialog, { DialogProps } from "@material-ui/core/Dialog";

export const Dialog = styled(MuiDialog).attrs({
  classes: {
    paper: "task-dialog-root"
  }
})`
  .task-dialog-root {
    overflow-y: visible;
  }
` as React.ComponentType<DialogProps>;
