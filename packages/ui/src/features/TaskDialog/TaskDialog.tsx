import React, { ChangeEvent, KeyboardEvent, useState } from "react";
import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormGroup,
  Grid
} from "@material-ui/core";
import { FieldArray, Form, Formik } from "formik";
import { TaskDetails, TaskNew } from "@react-modern-state-management/types";
import { newTaskSchema } from "@react-modern-state-management/schema";

import { getColor } from "@react-modern-state-management/utils";
import TextField from "../../components/form/TextField";
import KeyboardDateTimePicker from "../../components/form/KeyboardDateTimePicker";
import AutoCompleteField from "../../components/form/AutoCompleteField";
import TagList from "../../components/form/TagList";
import ColorPicker from "../../components/form/ColorPicker";
import SlideDown from "../../animations/SlideDown";

import { Dialog } from "./TaskDialog.style";

type TaskDialogProps = {
  task?: TaskDetails | null;
  categorySuggestions?: string[];
  title: string;
  actionText: string;
  open: boolean;
  toggle: () => void;
  enableReinitialize?: boolean;
  onSubmit: (task: TaskDetails | TaskNew) => void;
};

const newTask: TaskNew = {
  id: undefined,
  categories: [],
  index: -1,
  title: "",
  description: "",
  completed: false,
  dueDate: null,
  remindMeAt: null,
  color: "white"
};

const TaskDialog: React.FC<TaskDialogProps> = ({
  task,
  categorySuggestions = [],
  title,
  actionText,
  open,
  toggle,
  enableReinitialize,
  onSubmit
}) => {
  const editTask = task || newTask;
  const [categoryText, setCategoryText] = useState("");

  return (
    <Formik
      initialValues={editTask}
      enableReinitialize={enableReinitialize}
      validationSchema={newTaskSchema}
      onSubmit={(values, actions) => {
        onSubmit(values);
        actions.setSubmitting(false);
        actions.resetForm();
        toggle();
      }}
      render={props => {
        const { values, isSubmitting, setFieldValue } = props;
        const color = getColor(values.color);
        const { categories } = values;

        return (
          <Dialog
            open={open}
            TransitionComponent={SlideDown}
            onClose={toggle}
            aria-labelledby="form-dialog-title"
            maxWidth="sm"
          >
            <Box bgcolor={color}>
              <Form>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <DialogContent>
                  <fieldset style={{ border: "none" }} disabled={isSubmitting}>
                    <Grid container direction="column" spacing={2}>
                      <Grid item>
                        <TextField
                          name="title"
                          label="Title"
                          type="text"
                          fullWidth
                          // eslint-disable-next-line jsx-a11y/no-autofocus
                          autoFocus
                        />
                      </Grid>
                      <Grid item>
                        <TextField
                          name="description"
                          label="Description"
                          type="text"
                          rowsMax={10}
                          fullWidth
                          multiline
                        />
                      </Grid>
                      <Grid item>
                        <FormGroup row>
                          <Grid container direction="row" spacing={1}>
                            <Grid item>
                              <KeyboardDateTimePicker
                                name="dueDate"
                                label="Due date"
                                minDate={new Date()}
                                disabled={isSubmitting}
                                // @ts-ignore
                                onChange={setFieldValue}
                                clearable
                              />
                            </Grid>
                            <Grid item>
                              <KeyboardDateTimePicker
                                name="remindMeAt"
                                label="Remind me at"
                                disabled={!values.dueDate || isSubmitting}
                                minDate={new Date()}
                                maxDate={values.dueDate}
                                // @ts-ignore
                                onChange={setFieldValue}
                                clearable
                              />
                            </Grid>
                          </Grid>
                        </FormGroup>
                      </Grid>
                      <Grid item>
                        <FieldArray
                          name="categories"
                          render={arrayHelpers => (
                            <>
                              <AutoCompleteField
                                name="category"
                                value={categoryText}
                                onEnter={(value: string) => {
                                  const testValue = value.toLowerCase();
                                  if (
                                    categories.findIndex(
                                      item => item.toLowerCase() === testValue
                                    ) < 0
                                  ) {
                                    arrayHelpers.push(value);
                                    setCategoryText("");
                                  }
                                }}
                                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                  setCategoryText(e.currentTarget.value)
                                }
                                onKeyDown={(e: KeyboardEvent) => {
                                  if (e.keyCode === 13) e.preventDefault();
                                }}
                                suggestions={categorySuggestions.filter(
                                  // @ts-ignore
                                  tag => !categories.includes(tag)
                                )}
                                label="Categories"
                                fullWidth
                              />
                              <TagList
                                name="categories"
                                handleRemove={(index: number) => {
                                  arrayHelpers.remove(index);
                                }}
                              />
                            </>
                          )}
                        />
                      </Grid>
                      <Grid item>
                        <ColorPicker name="color" disabled={isSubmitting} />
                      </Grid>
                    </Grid>
                  </fieldset>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={toggle}
                    color="primary"
                    disabled={isSubmitting}
                  >
                    Cancel
                  </Button>
                  <Button type="submit" color="primary" disabled={isSubmitting}>
                    {actionText}
                  </Button>
                </DialogActions>
              </Form>
            </Box>
          </Dialog>
        );
      }}
    />
  );
};

export default TaskDialog;
