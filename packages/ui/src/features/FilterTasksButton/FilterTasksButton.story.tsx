import React, { useCallback, useState } from "react";

import { storiesOf } from "@storybook/react";

import FilterButton from "./FilterTasksButton";
import { getCategories } from "@react-modern-state-management/utils";
import { exampleTasks } from "@react-modern-state-management/api";
import { TaskFilters } from "@react-modern-state-management/types";

storiesOf("FilterButton", module).add(
  "Button with dialog",
  () => {
    const [filters, setFilters] = useState<TaskFilters>({
      text: undefined,
      completed: undefined,
      categories: undefined,
      dueDate: undefined,
      offset: undefined,
      limit: undefined
    });

    const allCategories = getCategories(exampleTasks);

    const handleFilterChange = useCallback(
      (filters: TaskFilters) => setFilters(filters),
      [setFilters]
    );

    return (
      <div
        style={{
          display: "inline-block",
          width: "50px",
          height: "50px",
          backgroundColor: "black"
        }}
      >
        <FilterButton
          categories={allCategories}
          filters={filters}
          onFiltersChange={handleFilterChange}
        />
      </div>
    );
  },
  { info: { inline: true } }
);
