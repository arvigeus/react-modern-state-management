import React, { useCallback, useState } from "react";
import { IconButton } from "@material-ui/core";

import { TaskFilters } from "@react-modern-state-management/types";
import FilterIcon from "./FilterIcon";
import FilterTasksDialog from "./FilterTasksDialog";

type FilterButtonProps = {
  categories: string[];
  filters: TaskFilters;
  onFiltersChange: (filters: TaskFilters) => void;
};

const FilterButton: React.FC<FilterButtonProps> = ({
  categories,
  filters,
  onFiltersChange
}) => {
  const [open, setOpen] = useState(false);
  const handleFilterToggle = useCallback(() => {
    setOpen(open => !open);
  }, [setOpen]);

  return (
    <>
      <IconButton onClick={handleFilterToggle}>
        <FilterIcon />
      </IconButton>

      <FilterTasksDialog
        filters={filters}
        categories={categories}
        open={open}
        toggle={handleFilterToggle}
        onFiltersChange={onFiltersChange}
      />
    </>
  );
};

export default FilterButton;
