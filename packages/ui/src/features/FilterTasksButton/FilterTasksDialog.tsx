import React, { useCallback } from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  FormControlLabel,
  FormGroup,
  Grid
} from "@material-ui/core";
import { FieldArray, Form, Formik, FormikHelpers } from "formik";

import { TaskFilters } from "@react-modern-state-management/types";

import TextField from "../../components/form/TextField";
import KeyboardDateTimePicker from "../../components/form/KeyboardDateTimePicker";
import Checkbox from "../../components/form/Checkbox";
import SlideDown from "../../animations/SlideDown";

type TaskFilterProps = {
  open: boolean;
  categories: string[];
  filters: TaskFilters;
  onFiltersChange: (filters: TaskFilters) => void;
  toggle: () => void;
};

const TaskFilter: React.FC<TaskFilterProps> = ({
  open,
  categories: allCategories,
  filters,
  onFiltersChange,
  toggle
}) => {
  const { text, categories, dueDate } = filters;

  const handleSubmit = useCallback(
    (values: TaskFilters, actions: FormikHelpers<TaskFilters>) => {
      onFiltersChange(values);
      actions.setSubmitting(false);
      actions.resetForm();
      toggle();
    },
    [onFiltersChange, toggle]
  );

  return (
    <Formik
      initialValues={{
        text: text || "",
        categories: categories || [],
        dueDate: dueDate || null
      }}
      enableReinitialize
      onSubmit={handleSubmit}
      render={props => {
        const { values, isSubmitting, setFieldValue, resetForm } = props;
        const { categories } = values;

        return (
          <Dialog
            open={open}
            TransitionComponent={SlideDown}
            onClose={toggle}
            aria-labelledby="form-dialog-title"
            maxWidth="sm"
          >
            <Box>
              <Form>
                <DialogTitle id="form-dialog-title">Filter</DialogTitle>
                <DialogContent>
                  <fieldset style={{ border: "none" }} disabled={isSubmitting}>
                    <Grid container direction="column" spacing={2}>
                      <Grid item>
                        <TextField
                          name="text"
                          label="Search for text containing..."
                          type="text"
                          fullWidth
                        />
                      </Grid>
                      <Grid item>
                        <FormControlLabel
                          label="Categories"
                          labelPlacement="top"
                          control={
                            <FormGroup row>
                              <Grid container direction="row" spacing={1}>
                                <FieldArray
                                  name="categories"
                                  render={arrayHelpers =>
                                    allCategories.map((category, index) => (
                                      <Grid key={category} item>
                                        <Checkbox
                                          name={`categories.${index}`}
                                          label={category}
                                          disabled={isSubmitting}
                                          checked={
                                            categories &&
                                            categories.includes(category)
                                          }
                                          onChange={(_, checked) => {
                                            if (checked)
                                              arrayHelpers.push(category);
                                            else if (categories)
                                              arrayHelpers.remove(
                                                categories.indexOf(category)
                                              );
                                          }}
                                        />
                                      </Grid>
                                    ))
                                  }
                                />
                              </Grid>
                            </FormGroup>
                          }
                        />
                        <Divider />
                      </Grid>
                      <Grid item>
                        <KeyboardDateTimePicker
                          name="dueDate"
                          label="Tasks before date..."
                          disabled={isSubmitting}
                          // @ts-ignore
                          onChange={setFieldValue}
                          clearable
                          fullWidth
                        />
                      </Grid>
                    </Grid>
                  </fieldset>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={() => resetForm()}
                    color="primary"
                    disabled={isSubmitting}
                  >
                    Reset
                  </Button>
                  <div style={{ flex: "1 0 0" }} />
                  <Button
                    onClick={toggle}
                    color="primary"
                    disabled={isSubmitting}
                  >
                    Cancel
                  </Button>
                  <Button type="submit" color="primary" disabled={isSubmitting}>
                    Search
                  </Button>
                </DialogActions>
              </Form>
            </Box>
          </Dialog>
        );
      }}
    />
  );
};

export default TaskFilter;
