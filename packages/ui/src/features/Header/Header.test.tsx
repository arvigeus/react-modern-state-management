import React from "react";
import { render, screen } from "test-utils";
import Header from "./Header";

test("Simple header", async () => {
  render(
    <Header
      title="Simple header"
      tabs={["one", "two", "three"]}
      onChange={() => {}}
    />
  );

  const header = screen.queryByTestId("header");
  const headerTitle = screen.queryByTestId("header-title");

  expect(header).not.toBeNull();
  expect(headerTitle).not.toBeNull();
  // expect(headerTitle).toHaveTextContent("Simple header");
});
