import React, { useCallback, useState } from "react";

import { storiesOf } from "@storybook/react";

import Header from "./Header";
import FilterIcon from "features/FilterTasksButton/FilterIcon";

storiesOf("Header", module)
  .add(
    "Tabs only",
    () => {
      const tabs = ["One", "Two", "Three"];

      const [activeTab, setActiveTab] = useState(null);

      const handleTabChange = useCallback(tab => setActiveTab(tab), [
        setActiveTab
      ]);

      return (
        <Header
          title="Tasks:"
          active={activeTab}
          tabs={tabs}
          onChange={handleTabChange}
        />
      );
    },
    { info: { inline: true } }
  )
  .add(
    "Tabs and button",
    () => {
      const tabs = ["One", "Two", "Three"];

      const [activeTab, setActiveTab] = useState(null);

      const handleTabChange = useCallback(tab => setActiveTab(tab), [
        setActiveTab
      ]);

      return (
        <Header
          title="Tasks:"
          active={activeTab}
          tabs={tabs}
          onChange={handleTabChange}
        >
          <FilterIcon />
        </Header>
      );
    },
    { info: { inline: true } }
  );
