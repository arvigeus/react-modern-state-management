import React from "react";
import styled from "styled-components/macro";
import { Box, Tabs as MuiTabs } from "@material-ui/core";
import { TabsProps } from "@material-ui/core/Tabs";

export const Wrapper = styled(Box)`
  transform: translateY(-20px);
`;

export const Tabs = styled(MuiTabs).attrs({
  classes: {
    root: "tab-root-button"
  }
})`
  &.tab-root-button {
    width: unset;
    display: inline-block;
    min-width: unset;
    max-width: unset;
    height: unset;
    min-height: unset;
    max-height: unset;
    border: 0;
    margin-left: 4px;
    border-radius: 3px;
    color: #fff;
    line-height: 24px;

    &:last-child {
      margin-left: 0;
    }
  }
` as React.ComponentType<TabsProps>;
