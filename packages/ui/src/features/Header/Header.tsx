import React, { useCallback } from "react";
import { Box, Tab } from "@material-ui/core";

import { Tabs, Wrapper } from "./Header.style";

type HeaderProps = {
  title: string;
  active?: string | null;
  tabs: string[];
  onChange: (tab: string) => void;
};

const Header: React.FC<HeaderProps> = ({
  title,
  active,
  tabs,
  onChange,
  children
}) => {
  const handleTabChange = useCallback(
    (_, value) => {
      onChange(tabs[value]);
    },
    [onChange, tabs]
  );

  const activeTabIndex = active ? tabs.indexOf(active) : 0;

  return (
    <Wrapper>
      <Box
        display="flex"
        alignItems="center"
        zIndex={3}
        borderRadius={4}
        bgcolor="primary.main"
        mx="15px"
        mt="-20px"
        p="15px"
        data-testid="header"
      >
        <Box
          display="inline-flex"
          mr={2}
          fontSize="h6.fontSize"
          fontWeight={500}
          color="white"
          data-testid="header-title"
        >
          {title}
        </Box>
        <Tabs
          value={activeTabIndex}
          // @ts-ignore
          onChange={handleTabChange}
          variant="scrollable"
          scrollButtons="auto"
          indicatorColor="secondary"
          data-testid="header-tabs"
        >
          {tabs.map(tab => (
            <Tab key={tab} label={tab} />
          ))}
        </Tabs>
        {children}
      </Box>
    </Wrapper>
  );
};

export default Header;
