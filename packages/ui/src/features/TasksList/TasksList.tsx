import React, { useCallback } from "react";
import {
  PaginationProps,
  TaskExcerpt
} from "@react-modern-state-management/types";
import VirtualDragAndDropList, {
  VirtualDragAndDropItem
} from "../../components/VirtualDragAndDropList";
import TasksListItem from "./TasksListItem";

const height = window.innerHeight - 170;
const itemHeight = 60;

type TasksListProps = {
  tasks: TaskExcerpt[];
  onTaskSelected: (id: string) => void;
  onTaskToggle: (id: string, completed: boolean) => void;
  onTaskMove: (startPosition: number, endPosition: number) => void;
  onTaskDelete: (id: string) => void;
} & PaginationProps;

const TasksList: React.FC<TasksListProps> = ({
  tasks,
  onTaskSelected,
  onTaskToggle,
  onTaskMove,
  onTaskDelete,
  hasNextPage,
  loadNextPage,
  isNextPageLoading
}) => {
  const sortedTasks = tasks.sort((a: TaskExcerpt, b: TaskExcerpt) =>
    // eslint-disable-next-line no-nested-ternary
    a.index > b.index ? 1 : b.index > a.index ? -1 : 0
  );

  const handleMove = useCallback(
    (startIndex, endIndex) => {
      // Send correct indexes even when using filters
      const start = tasks[startIndex].index;
      const end = tasks[endIndex].index;
      onTaskMove(start, end);
    },
    [onTaskMove, tasks]
  );

  return (
    <VirtualDragAndDropList
      droppableId="tasks"
      items={sortedTasks}
      hasNextPage={hasNextPage}
      loadNextPage={loadNextPage}
      isNextPageLoading={isNextPageLoading}
      onDragEnd={handleMove}
      height={height}
      itemHeight={itemHeight}
    >
      {(props: VirtualDragAndDropItem<TaskExcerpt>) => (
        <TasksListItem
          {...props}
          onTaskSelected={onTaskSelected}
          onTaskToggle={onTaskToggle}
          onTaskDelete={onTaskDelete}
        />
      )}
    </VirtualDragAndDropList>
  );
};

export default TasksList;
