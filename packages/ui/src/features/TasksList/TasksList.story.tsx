import React, { useCallback, useState } from "react";

import { storiesOf } from "@storybook/react";

import TasksList from "./TasksList";
import Header from "features/Header";
import FilterTasksButton from "features/FilterTasksButton";
import TaskDialog from "features/TaskDialog";
import {
  filterTasks,
  getCategories,
  moveTasks
} from "@react-modern-state-management/utils";
import { exampleTasks } from "@react-modern-state-management/api";
import {
  TaskDetails,
  TaskFilters,
  TaskNew
} from "@react-modern-state-management/types";

storiesOf("TasksList", module).add(
  "Full demo",
  () => {
    const [tasks, setTasks] = useState<TaskDetails[]>(exampleTasks);
    const [filters, setFilters] = useState<TaskFilters>({
      text: undefined,
      completed: undefined,
      categories: undefined,
      dueDate: undefined,
      offset: undefined,
      limit: undefined
    });

    const [activeCategory, setActiveCategory] = useState(null);

    const [selected, setSelected] = useState<TaskDetails | null>(null);

    const handleCategoryTabChange = useCallback(
      category => setActiveCategory(category),
      [setActiveCategory]
    );

    const handleFilterChange = useCallback(
      (filters: TaskFilters) => setFilters(filters),
      [setFilters]
    );

    const handleTaskSelected = useCallback(
      id => {
        // @ts-ignore
        setSelected(tasks.find(task => task.id === id));
      },
      [tasks]
    );

    const handleTaskToggle = useCallback(
      (id, completed) => {
        setTasks(
          tasks.map(task => (task.id === id ? { ...task, completed } : task))
        );
      },
      [tasks]
    );

    const handleTaskDelete = useCallback(
      id => {
        setTasks(tasks.filter(task => task.id !== id));
      },
      [tasks]
    );

    const handleDialogToggle = useCallback(() => {
      setSelected(null);
    }, []);

    const handleTaskMove = useCallback(
      (startIndex, endIndex) => {
        setTasks(moveTasks(tasks, startIndex, endIndex));
      },
      [tasks]
    );

    const handleSubmit = useCallback(
      (submitted: TaskDetails | TaskNew) => {
        setTasks(
          tasks.map(task =>
            task.id === submitted.id ? { ...task, ...submitted } : task
          )
        );
      },
      [tasks]
    );

    const filteredTasks = filterTasks(tasks, filters);

    const allCategories = getCategories(tasks);
    const headerTabs = getCategories(filteredTasks);

    return (
      <>
        <Header
          title="Tasks:"
          active={activeCategory}
          tabs={headerTabs}
          onChange={handleCategoryTabChange}
        >
          <FilterTasksButton
            categories={allCategories}
            filters={filters}
            onFiltersChange={handleFilterChange}
          />
        </Header>
        <TasksList
          tasks={filteredTasks}
          onTaskSelected={handleTaskSelected}
          onTaskToggle={handleTaskToggle}
          onTaskMove={handleTaskMove}
          onTaskDelete={handleTaskDelete}
          hasNextPage={false}
          loadNextPage={(_: number, __: number) => Promise.resolve()}
          isNextPageLoading={false}
        />
        <TaskDialog
          task={selected}
          categorySuggestions={allCategories}
          title="Edit task"
          actionText="Submit"
          open={selected !== null}
          toggle={handleDialogToggle}
          enableReinitialize
          onSubmit={handleSubmit}
        />
      </>
    );
  },
  { info: { inline: true } }
);
