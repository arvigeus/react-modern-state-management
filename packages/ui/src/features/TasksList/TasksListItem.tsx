import React, { ChangeEvent, useCallback } from "react";
import { Box, Checkbox } from "@material-ui/core";
import { darken } from "@material-ui/core/styles/colorManipulator";
import { areEqual } from "react-window";

import { formatDate, getColor } from "@react-modern-state-management/utils";
import { VirtualDragAndDropItem } from "../../components/VirtualDragAndDropList";

import { TaskExcerpt } from "@react-modern-state-management/types";
import {
  ActionButton,
  AlarmIcon,
  CellActions,
  CellCheck,
  CellTask,
  DeleteIcon,
  Description,
  Reminder,
  Title,
  Tooltip
} from "./TasksListItem.style";

export type TaskItemProps = VirtualDragAndDropItem<TaskExcerpt> & {
  onTaskSelected: (id: string) => void;
  onTaskToggle: (id: string, completed: boolean) => void;
  onTaskDelete: (id: string) => void;
};

const TasksListItem: React.FC<TaskItemProps> = ({
  item,
  provided,
  isDragging,
  style,
  onTaskSelected,
  onTaskDelete,
  onTaskToggle
}) => {
  const {
    id,
    title,
    description,
    completed,
    dueDate,
    remindMeAt,
    color: colorName
  } = item;
  const color = getColor(colorName);

  const handleToggle = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      onTaskToggle(e.target.value, e.target.checked);
    },
    [onTaskToggle]
  );

  const handleSelect = useCallback(() => {
    onTaskSelected(id);
  }, [id, onTaskSelected]);

  const handleDelete = useCallback(() => {
    onTaskDelete(id);
  }, [id, onTaskDelete]);

  const reminder = dueDate && (
    <Reminder>
      {formatDate(dueDate)} {remindMeAt && <AlarmIcon />}
    </Reminder>
  );

  return (
    // @ts-ignore
    <Box
      bgcolor={color}
      display="flex"
      flexDirection="row"
      flexWrap="nowrap"
      position="relative"
      borderBottom={`1px solid ${darken(color, 0.2)}`}
      // @ts-ignore
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      // @ts-ignore
      style={{ ...provided.draggableProps.style, ...style }}
      data-is-dragging={isDragging}
    >
      {reminder}
      <CellCheck>
        <Checkbox
          checked={completed}
          tabIndex={-1}
          onChange={handleToggle}
          value={id}
          id={id}
        />
      </CellCheck>
      <CellTask onClick={handleSelect}>
        <Title>{title}</Title>
        {description && <Description>{description}</Description>}
      </CellTask>
      <CellActions>
        <Tooltip id="tooltip-top-start" title="Remove" placement="top">
          <ActionButton
            aria-label="Delete"
            color="primary"
            onClick={handleDelete}
          >
            <DeleteIcon />
          </ActionButton>
        </Tooltip>
      </CellActions>
    </Box>
  );
};

export default React.memo(TasksListItem, areEqual);
