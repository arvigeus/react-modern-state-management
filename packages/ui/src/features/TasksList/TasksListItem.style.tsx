import React from "react";
import styled from "styled-components/macro";
import { IconButton, Tooltip as MuiTooltip } from "@material-ui/core";
import { TooltipProps } from "@material-ui/core/Tooltip";
import { IconButtonProps } from "@material-ui/core/IconButton";
import { AlarmOn, Delete } from "@material-ui/icons";

export const Reminder = styled.div`
  position: absolute;
  top: 0;
  right: 5px;
  color: #656565;
  font-size: 11px;
`;

export const CellCheck = styled.div`
  position: relative;
  flex: 0 1;
  padding: 8px;
`;

export const CellTask = styled.div`
  position: relative;
  overflow: hidden;
  flex: 1 1 auto;
  padding: 8px;
  user-select: none;
`;

export const Title = styled.span`
  font-size: 14px;
  line-height: 42px;
  vertical-align: middle;
`;

export const Description = styled.small`
  position: absolute;
  bottom: 0;
  display: block;
  overflow: hidden;
  width: 100%;
  padding-right: 4px;
  font-size: 10px;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const CellActions = styled.div`
  display: flex;
  flex: 0 1;
  align-items: center;
  padding: 8px;
`;

export const Tooltip = styled(MuiTooltip).attrs({
  classes: {
    tooltip: "tooltip"
  }
})`
  .tooltip {
    min-width: 130px;
    max-width: 200px;
    padding: 10px 15px;
    border: none;
    border-radius: 3px;
    box-shadow: 0 8px 10px 1px rgba(0, 0, 0, 0.14),
      0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2);
    font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
    letter-spacing: normal;
    line-break: auto;
    line-height: 1.7em;
    text-align: center;
    text-shadow: none;
    text-transform: none;
    white-space: normal;
    word-spacing: normal;
    word-wrap: normal;
  }
` as React.ComponentType<TooltipProps>;

export const ActionButton = styled(IconButton)`
  width: 36px;
  height: 36px;
  padding: 0;
` as React.ComponentType<IconButtonProps>;

export const DeleteIcon = styled(Delete)`
  width: 15px;
  height: 15px;
  background-color: transparent;
  box-shadow: none;
` as React.ComponentType<any>;

export const AlarmIcon = styled(AlarmOn)`
  width: 12px;
  height: 12px;
` as React.ComponentType<any>;
