import React from "react";
import styled from "styled-components/macro";
import { Fab } from "@material-ui/core";
import { Add as AddIcon } from "@material-ui/icons";
import usePortal from "react-useportal";

type AddTaskButonProps = {
  parentId?: string;
  onClick: React.MouseEventHandler;
};

/**
 * This will be rendered outside its parents as workaround to
 * `position: fixed` not working when parent is having transform
 * @param {AddTaskButonProps} Just needs a onClick handler
 * @returns {React.Element} A material FAB button
 */
const AddTaskButon: React.FC<AddTaskButonProps> = ({
  parentId = "app-fab",
  onClick
}) => {
  const { Portal } = usePortal();
  return (
    // @ts-ignore
    <Portal bindTo={document && document.getElementById(parentId)}>
      <FabWrapper>
        <Fab color="primary" aria-label="Add" onClick={onClick}>
          <AddIcon />
        </Fab>
      </FabWrapper>
    </Portal>
  );
};

const FabWrapper = styled.div`
  position: fixed;
  right: 30px;
  bottom: 30px;
`;

export default AddTaskButon;
