export type TaskExcerpt = {
  id: string;
  index: number;
  categories: string[];
  title: string;
  description: string | null;
  completed: boolean;
  dueDate: Date | null;
  remindMeAt: Date | null;
  color: string | null;
};

export type TaskDetails = {} & TaskExcerpt;

export interface TaskNew extends Omit<TaskDetails, "id"> {
  id?: string;
}

export interface TaskUpdate extends Partial<TaskDetails> {
  id: string;
}

export type TasksMoved = { [key: string]: number };

export type TaskUnknown = {
  id: string;
};

export type TaskFilters = {
  text?: string;
  completed?: boolean;
  categories?: string[];
  dueDate?: Date | null;
  offset?: number;
  limit?: number;
};
