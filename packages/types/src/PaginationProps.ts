export type PaginationProps = {
  hasNextPage: boolean;
  loadNextPage: (startIndex: number, stopIndex: number) => Promise<any>;
  isNextPageLoading: boolean;
};
