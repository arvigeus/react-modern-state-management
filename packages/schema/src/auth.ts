import * as yup from "yup";

export const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required(),
  password: yup
    .string()
    .min(6)
    .required(),
  website: yup.string()
});

export const registerSchema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required(),
  password: yup.string().required("Password is required"),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password"), ""], "Passwords must match"),
  website: yup.string()
});
