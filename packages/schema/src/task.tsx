import * as yup from "yup";

export const categoryValidation = yup
  .string()
  .min(3, "Category must be at least 3 characters")
  .max(15, "Category must be at most 15 characters");

export const newTaskSchema = yup.object().shape({
  title: yup
    .string()
    .min(3, "Give a meaningful title")
    .required(),
  description: yup.string(),
  dueDate: yup.date().nullable(),
  remindMeAt: yup.date().nullable(),
  category: categoryValidation,
  categories: yup.array().of(categoryValidation),
  color: yup
    .string()
    .matches(/(white|green|red|blue|pink|orange|yellow|silver)/, "A new color?")
});
