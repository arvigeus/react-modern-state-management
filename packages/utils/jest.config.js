module.exports = {
  transform: {
    ".(ts|tsx)": "ts-jest"
  },
  testMatch: ["<rootDir>/src/**/*.(spec|test).{ts,tsx}"],
  setupTestFrameworkScriptFile: "jest-extended",
  collectCoverageFrom: ["<rootDir>/src/**/*.{ts,tsx}"],
  coverageReporters: ["lcov", "text-summary"]
};
