import { TaskFilters } from "@react-modern-state-management/types";
import { filter } from "fuzzaldrin";

export const filterTasks = <
  T extends {
    title: string;
    description: string | null;
    completed: boolean;
    categories: string[];
  }
>(
  tasks: T[],
  filters: TaskFilters
) =>
  tasks.filter(task => {
    if (
      filters.text &&
      filter([task.title, task.description || ""], filters.text).length === 0
    )
      return false;

    if (filters.completed !== undefined && task.completed !== filters.completed)
      return false;

    if (
      filters.categories &&
      filters.categories.filter(value => task.categories.includes(value))
        .length === 0
    )
      return false;

    return true;
  });

/**
 * Moves an element to index and updates the rest elements to have correct index too
 *
 * @remarks
 * This method does not actually change the order of elements, it merely updates their index
 *
 * @param tasks - List of elements to be updated
 * @param startIndex - Index of the target element to be moved
 * @param endIndex - The end index that the target element will be moved
 * @returns Updated list with changed indexes
 */
export const moveTasks = <T extends { index: number }>(
  tasks: T[],
  startIndex: number,
  endIndex: number
) => {
  const left = Math.min(startIndex, endIndex);
  const right = Math.max(startIndex, endIndex);
  const dir = Math.sign(startIndex - endIndex);
  return tasks.map(task => {
    if (task.index === startIndex) return { ...task, index: endIndex };
    if (left <= task.index && task.index <= right)
      return { ...task, index: task.index + dir };
    return task;
  });
};

/**
 * Update tasks indexes based on a json object
 *
 * @param tasks - List of tasks to be parsed
 * @param updated - Key is task id, value is new index
 * @returns Array of unique categories
 */
export const updateTaskIndexes = <T extends { id: string; index: number }>(
  tasks: T[],
  updated: { [key: string]: number }
) =>
  tasks.map(task => {
    const index = updated[task.id];
    if (index != null) return { ...task, index };
    return task;
  });

/**
 * Collects all unique categories from a task list
 *
 * @param tasks - List of tasks to be parsed
 * @returns Array of unique categories
 */
export const getCategories = <T extends { categories: string[] }>(tasks: T[]) =>
  Array.from(
    tasks.reduce((categories, task) => {
      task.categories.forEach(category => {
        categories.add(category);
      });
      return categories;
    }, new Set<string>())
  );
