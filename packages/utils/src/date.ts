const currentYear = new Date().getFullYear();
export const formatDate = (date: Date) =>
  date.toLocaleDateString(navigator.language, {
    weekday: "long",
    year: date.getFullYear() === currentYear ? undefined : "numeric",
    month: "long",
    day: "numeric"
  });
