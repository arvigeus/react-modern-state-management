import { formatDate } from "./date";

describe("formatDate", () => {
  it("returns short format when current year", () => {
    const currentYear = new Date().getFullYear();
    const testDate = new Date(currentYear, 2, 15);
    const result = formatDate(testDate);
    expect(result).toEqual("Friday, March 15");
  });
  it("returns full format when different year", () => {
    const lastYear = new Date().getFullYear() - 1;
    const testDate = new Date(lastYear, 2, 15);
    const result = formatDate(testDate);
    expect(result).toEqual(`Thursday, March 15, ${lastYear}`);
  });
});
