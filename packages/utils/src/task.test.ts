import "jest-extended";
import { filterTasks, getCategories, moveTasks } from "./task";

const tasks = [
  {
    title: "Setup a monorepo",
    description: "Use Lerna and Yarn workspaces",
    completed: true,
    categories: ["infrastructure"]
  },
  {
    title: "Integrate with Visual Studio Code",
    description: "Needs to work with debugger and use hotkeys to start",
    completed: false,
    categories: ["infrastructure"]
  },
  {
    title: "Configure TypeScript",
    description: "Make sure it works with project references",
    completed: true,
    categories: ["infrastructure"]
  },
  {
    title: "Use Storybook",
    description: null,
    completed: true,
    categories: ["ui", "testing"]
  },
  {
    title: "Write tests with Jest",
    description: "Integration with VS Code needs some working",
    completed: false,
    categories: ["testing"]
  },
  {
    title: "End to End automation testing with Cypress",
    description: null,
    completed: false,
    categories: ["testing"]
  },
  {
    title: "Create packages for React state management providers",
    description: "Redux, MobX, Apollo, Relay, and others",
    completed: false,
    categories: ["ui"]
  }
];

describe("filterTasks", () => {
  it("finds completed tasks", () => {
    const result = filterTasks(tasks, { completed: true });
    const expected = [tasks[0], tasks[2], tasks[3]];
    expect(result).toHaveLength(expected.length);
    for (const task of expected) expect(result).toContainEqual(task);
  });
  it("finds tasks by text", () => {
    const result = filterTasks(tasks, { text: "code" });
    const expected = [tasks[1], tasks[4], tasks[6]];
    expect(result).toHaveLength(expected.length);
    for (const task of expected) expect(result).toContainEqual(task);
  });
  it("finds tasks by categories", () => {
    const result = filterTasks(tasks, { categories: ["ui", "testing"] });
    const expected = [tasks[3], tasks[4], tasks[5], tasks[6]];
    expect(result).toHaveLength(expected.length);
    for (const task of expected) expect(result).toContainEqual(task);
  });
  it("finds tasks by multiple filters", () => {
    const result = filterTasks(tasks, {
      categories: ["infrastructure"],
      completed: true
    });
    const expected = [tasks[0], tasks[2]];
    expect(result).toHaveLength(expected.length);
    for (const task of expected) expect(result).toContainEqual(task);
  });
});

describe("moveTasks", () => {
  it("changes indexes when moving from left to right", () => {
    const tasks = [
      { id: "1", index: 1 },
      { id: "2", index: 2 },
      { id: "3", index: 3 },
      { id: "4", index: 4 },
      { id: "5", index: 5 },
      { id: "6", index: 6 }
    ];
    const expected = [
      { id: "1", index: 1 },
      { id: "3", index: 2 },
      { id: "4", index: 3 },
      { id: "5", index: 4 },
      { id: "2", index: 5 },
      { id: "6", index: 6 }
    ];

    const result = moveTasks(tasks, 2, 5);
    for (const expectedTask of expected) {
      const resultTask = result.find(task => task.id === expectedTask.id);
      expect(resultTask).not.toBeNull();
      expect(resultTask!.index).toEqual(expectedTask.index);
    }
  });
  it("changes indexes when moving from right to left", () => {
    const tasks = [
      { id: "1", index: 1 },
      { id: "2", index: 2 },
      { id: "3", index: 3 },
      { id: "4", index: 4 },
      { id: "5", index: 5 },
      { id: "6", index: 6 }
    ];
    const expected = [
      { id: "1", index: 1 },
      { id: "5", index: 2 },
      { id: "2", index: 3 },
      { id: "3", index: 4 },
      { id: "4", index: 5 },
      { id: "6", index: 6 }
    ];

    const result = moveTasks(tasks, 5, 2);
    for (const expectedTask of expected) {
      const resultTask = result.find(task => task.id === expectedTask.id);
      expect(resultTask).not.toBeNull();
      expect(resultTask!.index).toEqual(expectedTask.index);
    }
  });
});

describe("getCategories", () => {
  it("returns all unique entries", () => {
    const tasks = [
      { categories: ["one"] },
      { categories: ["two", "three"] },
      { categories: ["one", "three"] },
      { categories: ["two", "four"] }
    ];
    const expected = ["one", "two", "three", "four"];

    const result = getCategories(tasks);
    expect(result).toIncludeAllMembers(expected);
  });
});
