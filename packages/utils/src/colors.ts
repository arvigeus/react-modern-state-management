export type ColorType =
  | "white"
  | "green"
  | "red"
  | "blue"
  | "pink"
  | "orange"
  | "yellow"
  | "silver"
  | null
  | undefined
  | string;

const colors = {
  white: "#fff",
  green: "#a0cd67",
  red: "#ff6961",
  blue: "#6abae5",
  pink: "#ce839a",
  orange: "#eea35a",
  yellow: "#fada5e",
  silver: "#ddd"
};

export const entries = Object.entries(colors);

export const getColor = (color: ColorType): string => {
  if (!color || !colors.hasOwnProperty(color)) return colors.white;
  // @ts-ignore
  return colors[color];
};

export default colors;
