import { useLocation } from "react-router-dom";
import { useTransition } from "react-spring";
import { Location } from "history";

export const useFadeTransition = () => {
  const location = useLocation();

  const animate =
    (location.state && location.state.animation === "fade") ||
    location.pathname === "/";

  const transitions = useTransition(
    location,
    (location: Location) => location.pathname,
    {
      // @ts-ignore
      native: true,
      initial: null,
      immediate: !animate,
      from: {
        opacity: 0,
        position: "absolute",
        minWidth: "100vw",
        minHeight: "100vh",
        top: 0,
        left: 0
      },
      enter: { opacity: 1, position: "initial" },
      leave: {
        opacity: 0,
        position: "absolute",
        minWidth: "100vw",
        minHeight: "100vh",
        top: 0,
        left: 0
      }
    }
  );

  return transitions;
};

export const usePageTurnTransition = () => {
  const location = useLocation();

  const animationName = location.state ? location.state.animation : undefined;
  const animate =
    animationName === "turn-page-left" || animationName === "turn-page-right";
  const sign = animationName === "turn-page-left" ? -1 : 1;
  const animation = animate
    ? {
        from: {
          opacity: 0,
          transform: `perspective(2000px) rotateY(${90 *
            sign}deg) scale(1.5) translateX(${75 * sign}px)`
        },
        enter: [
          {
            opacity: 1,
            transform:
              "perspective(2000px) rotateY(0deg) scale(1) translateX(0)"
          },
          {
            transform: "none",
            immediate: true
          }
        ],
        leave: {
          position: "absolute",
          top: 0,
          opacity: 0,
          pointerEvents: "none"
        }
      }
    : { from: {}, enter: { transform: "none" }, leave: {} };

  const transitions = useTransition(
    location,
    (location: Location) => location.pathname,
    {
      // @ts-ignore
      native: true,
      // initial: null,
      immediate: !animate,
      ...animation
    }
  );

  return transitions;
};
