import React from "react";
import { Route, RouteComponentProps } from "react-router-dom";
import { Transition, animated } from "react-spring";

export const FadeRoute: React.FC = ({ children }) => (
  <Route
    render={({ location }: RouteComponentProps<any>) => {
      const animate =
        (location.state && location.state.animation === "fade") ||
        location.pathname === "/";
      return (
        // @ts-ignore
        <Transition
          // @ts-ignore
          native
          initial={null}
          immediate={!animate}
          items={location}
          keys={location => location.pathname}
          from={{
            opacity: 0,
            position: "absolute",
            minWidth: "100vw",
            minHeight: "100vh",
            top: 0,
            left: 0
          }}
          enter={{ opacity: 1, position: "initial" }}
          leave={{
            opacity: 0,
            position: "absolute",
            minWidth: "100vw",
            minHeight: "100vh",
            top: 0,
            left: 0
          }}
        >
          {location => style => (
            // @ts-ignore
            <animated.div style={style}>{children(location)}</animated.div>
          )}
        </Transition>
      );
    }}
  />
);

export const PageTurnRoute: React.FC = ({ children }) => (
  <Route
    render={({ location }: RouteComponentProps<any>) => {
      const animationName = location.state
        ? location.state.animation
        : undefined;
      const animate =
        animationName === "turn-page-left" ||
        animationName === "turn-page-right";
      const sign = animationName === "turn-page-left" ? -1 : 1;
      const animation = animate
        ? {
            from: {
              opacity: 0,
              transform: `perspective(2000px) rotateY(${90 *
                sign}deg) scale(1.5) translateX(${75 * sign}px)`
            },
            enter: [
              {
                opacity: 1,
                transform:
                  "perspective(2000px) rotateY(0deg) scale(1) translateX(0)"
              },
              {
                transform: "none",
                immediate: true
              }
            ],
            leave: {
              position: "absolute",
              top: 0,
              opacity: 0,
              pointerEvents: "none"
            }
          }
        : { from: {}, enter: { transform: "none" }, leave: {} };

      return (
        // @ts-ignore
        <Transition
          // @ts-ignore
          native
          // initial={null}
          immediate={!animate}
          items={location}
          keys={location => location.pathname}
          {...animation}
        >
          {location => style => (
            // @ts-ignore
            <animated.div style={style}>{children(location)}</animated.div>
          )}
        </Transition>
      );
    }}
  />
);
