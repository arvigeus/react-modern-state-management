import styled from "styled-components/macro";
import { Link as Href } from "react-router-dom";

export const Container = styled.div`
  position: static;
  display: flex;
  height: 100vh;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-around;
  background-color: #282c34;
`;

export const Item = styled.div`
  padding: 20px;
  color: #fff !important;
  text-align: center;
  text-decoration: none !important;
  transition: transform 0.25s;

  &:hover {
    transform: scale(1.2);
  }
`;

export const Link = styled(Href)`
  color: #fff !important;
  font-size: calc(10px + 2vmin);
  text-decoration: none !important;
`;

export const PlainLink = styled.a`
  color: #fff !important;
  font-size: calc(10px + 2vmin);
  text-decoration: none !important;
`;
