import React from "react";
import { Typography } from "@material-ui/core";

import { Container, Item, PlainLink } from "./HomeLayout.style";
import GithubLogo from "components/GithubLogo";
import * as Providers from "providers";

const providers = Object.values(Providers);

const Home: React.FC = () => (
  <>
    <Container>
      {providers.map(({ url, name, logoSrc }) => (
        <Item key={name}>
          <PlainLink href={url}>
            {/* <Link to={{ pathname: url, state: { animation: "fade" } }}> */}
            <img src={logoSrc} alt={name} width="128px" height="128px" />
            <Typography variant="h6">{name}</Typography>
            {/* </Link> */}
          </PlainLink>
        </Item>
      ))}
    </Container>
    <GithubLogo link="https://github.com/arvigeus/react-modern-state-management" />
  </>
);

export default Home;
