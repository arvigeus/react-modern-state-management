import React from "react";
import { Auth0Provider } from "@react-modern-state-management/auth";
import { UIProvider } from "@react-modern-state-management/ui";
import StylesProvider from "theme/StylesProvider";

import Pages from "./pages";

// A function that routes the user to the right place
// after login
const onRedirectCallback = (appState: any) => {
  window.history.replaceState(
    {},
    document.title,
    appState?.targetUrl ? appState.targetUrl : window.location.pathname
  );
};

const App = () => (
  <Auth0Provider
    domain={process.env.REACT_APP_AUTH0_DOMAIN}
    client_id={process.env.REACT_APP_AUTH0_CLIENTID}
    redirect_uri={window.location.origin}
    onRedirectCallback={onRedirectCallback}
  >
    <StylesProvider>
      <UIProvider>
        <Pages />
      </UIProvider>
    </StylesProvider>
  </Auth0Provider>
);

export default App;
