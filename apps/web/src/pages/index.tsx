import React, { Suspense, lazy } from "react";
import { Route, Switch } from "react-router-dom";
import { FadeRoute } from "components/AnimatedRoute";

const Home = lazy(() => import("./home"));

const Pages: React.FC = () => (
  <Suspense fallback={null}>
    <Route exact path="/" component={Home} />
  </Suspense>
);

export default Pages;
