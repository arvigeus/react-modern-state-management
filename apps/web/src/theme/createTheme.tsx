import { createMuiTheme } from "@material-ui/core/styles";

const createTheme = (color: string, altColor: string) =>
  createMuiTheme({
    palette: {
      common: {
        black: "#495057",
        white: "#eee"
      },
      primary: { main: color },
      secondary: { main: altColor },
      background: {
        default: "linear-gradient(60deg, #764abc, #8a65c6)"
      }
    }
  });

export default createTheme;
