import React from "react";
import { createGlobalStyle } from "styled-components/macro";
import { StylesProvider as MuiStylesProvider } from "@material-ui/styles";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import CssBaseline from "@material-ui/core/CssBaseline";
import DateFnsUtils from "@date-io/date-fns";

const Style = createGlobalStyle`
  html * {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    margin: 0;
    background-color: #eee;
    color: #3c4858;
    font-family: Roboto, Helvetica, Arial, sans-serif;
    font-weight: 300;
    line-height: 1.5em;
  }
`;

const StylesProvider: React.FC = ({ children }) => (
  <MuiStylesProvider injectFirst>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <CssBaseline />
      <Style />
      {children}
    </MuiPickersUtilsProvider>
  </MuiStylesProvider>
);

export default StylesProvider;
