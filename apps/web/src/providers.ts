export type ProviderType = {
  name: string;
  url: string;
  logoSrc: string;
  colors: {
    primary: string;
    secondary: string;
  };
};

export const Redux: ProviderType = {
  name: "Redux",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/redux.svg"),
  colors: {
    primary: "#764abc",
    secondary: "#ca2929"
  }
};

export const MobX: ProviderType = {
  name: "MobX",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/mobx.svg"),
  colors: {
    primary: "#de5c16",
    secondary: "#2985ca"
  }
};

export const Apollo: ProviderType = {
  name: "Apollo",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/apollo.svg"),
  colors: {
    primary: "#112B49",
    secondary: "#ca7d29"
  }
};

export const Relay: ProviderType = {
  name: "Relay",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/relay.svg"),
  colors: {
    primary: "#764abc",
    secondary: "#ca2929"
  }
};

export const ReactHooksAndContext: ProviderType = {
  name: "Hooks + Context",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/react.svg"),
  colors: {
    primary: "#764abc",
    secondary: "#ca2929"
  }
};

export const RxJs: ProviderType = {
  name: "RxJs",
  url: `${process.env.PUBLIC_URL}/under-construction/`,
  logoSrc: require("./images/rxjs.svg"),
  colors: {
    primary: "#764abc",
    secondary: "#ca2929"
  }
};
