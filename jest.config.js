module.exports = {
  projects: [
    "<rootDir>/packages/api/jest.config.js",
    "<rootDir>/packages/auth/jest.config.js",
    "<rootDir>/packages/schema/jest.config.js",
    "<rootDir>/packages/types/jest.config.js",
    "<rootDir>/packages/ui/jest.config.js",
    "<rootDir>/packages/utils/jest.config.js"
  ]
};
